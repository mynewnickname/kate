/*
    SPDX-FileCopyrightText: 2021 Waqar Ahmed <waqar.17a@gmail.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
#include "stashdialog.h"
#include "git/gitutils.h"
#include "gitwidget.h"

#include <QCoreApplication>
#include <QFileInfo>
#include <QKeyEvent>
#include <QLineEdit>
#include <QPainter>
#include <QProcess>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QTemporaryFile>
#include <QTextDocument>
#include <QTreeView>
#include <QVBoxLayout>
#include <QWidget>
#include <QtConcurrentRun>

#include <KTextEditor/MainWindow>
#include <KTextEditor/Message>
#include <KTextEditor/View>

#include <KLocalizedString>

#include <kfts_fuzzy_match.h>

constexpr int StashIndexRole = Qt::UserRole + 2;

class StashFilterModel : public QSortFilterProxyModel
{
public:
    StashFilterModel(QObject *parent = nullptr)
        : QSortFilterProxyModel(parent)
    {
    }

    Q_SLOT void setFilterString(const QString &string)
    {
        beginResetModel();
        m_pattern = string;
        endResetModel();
    }

protected:
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override
    {
        return sourceLeft.data(FuzzyScore).toInt() < sourceRight.data(FuzzyScore).toInt();
    }

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override
    {
        if (m_pattern.isEmpty()) {
            return true;
        }

        int score = 0;
        const auto idx = sourceModel()->index(sourceRow, 0, sourceParent);
        const QString string = idx.data().toString();
        const bool res = kfts::fuzzy_match(m_pattern, string, score);
        sourceModel()->setData(idx, score, FuzzyScore);
        return res;
    }

private:
    QString m_pattern;
    static constexpr int FuzzyScore = Qt::UserRole + 1;
};

class StyleDelegate : public QStyledItemDelegate
{
public:
    StyleDelegate(QObject *parent = nullptr)
        : QStyledItemDelegate(parent)
    {
    }

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override
    {
        QStyleOptionViewItem options = option;
        initStyleOption(&options, index);

        QString name = index.data().toString();

        QVector<QTextLayout::FormatRange> formats;

        int colon = name.indexOf(QLatin1Char(':'));
        QString stashMessage = name.mid(colon + 1, name.length() - (colon + 1));
        ++colon;

        QTextCharFormat bold;
        bold.setFontWeight(QFont::Bold);
        formats.append({0, colon, bold});

        QTextCharFormat fmt;
        fmt.setForeground(options.palette.link());
        fmt.setFontWeight(QFont::Bold);
        auto resFmts = kfts::get_fuzzy_match_formats(m_filterString, stashMessage, colon, fmt);

        formats.append(resFmts);

        painter->save();

        //        // paint background
        if (option.state & QStyle::State_Selected) {
            painter->fillRect(option.rect, option.palette.highlight());
        } else {
            painter->fillRect(option.rect, option.palette.base());
        }

        options.text = QString(); // clear old text
        options.widget->style()->drawControl(QStyle::CE_ItemViewItem, &options, painter, options.widget);

        kfts::paintItemViewText(painter, name, options, formats);

        painter->restore();
    }

public Q_SLOTS:
    void setFilterString(const QString &text)
    {
        m_filterString = text;
    }

private:
    QString m_filterString;
};

StashDialog::StashDialog(QWidget *parent, KTextEditor::MainWindow *mainWindow)
    : QMenu(parent)
    , m_mainWindow(mainWindow)
    , m_gitwidget(qobject_cast<GitWidget *>(parent))
{
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setContentsMargins(4, 4, 4, 4);
    setLayout(layout);

    m_lineEdit = new QLineEdit(this);
    setFocusProxy(m_lineEdit);

    layout->addWidget(m_lineEdit);

    m_treeView = new QTreeView();
    layout->addWidget(m_treeView, 1);
    m_treeView->setTextElideMode(Qt::ElideLeft);
    m_treeView->setUniformRowHeights(true);

    m_model = new QStandardItemModel(this);

    StyleDelegate *delegate = new StyleDelegate(this);
    m_treeView->setItemDelegateForColumn(0, delegate);

    m_proxyModel = new StashFilterModel(this);
    m_proxyModel->setFilterRole(Qt::DisplayRole);

    connect(m_lineEdit, &QLineEdit::returnPressed, this, &StashDialog::slotReturnPressed);
    connect(m_lineEdit, &QLineEdit::textChanged, m_proxyModel, &StashFilterModel::setFilterString);
    connect(m_lineEdit, &QLineEdit::textChanged, delegate, &StyleDelegate::setFilterString);
    connect(m_lineEdit, &QLineEdit::textChanged, this, [this]() {
        m_treeView->viewport()->update();
        reselectFirst();
    });
    connect(m_treeView, &QTreeView::clicked, this, &StashDialog::slotReturnPressed);

    m_proxyModel->setSourceModel(m_model);
    m_treeView->setSortingEnabled(true);
    m_treeView->setModel(m_proxyModel);

    m_treeView->installEventFilter(this);
    m_lineEdit->installEventFilter(this);

    m_treeView->setHeaderHidden(true);
    m_treeView->setRootIsDecorated(false);
    m_treeView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_treeView->setSelectionMode(QTreeView::SingleSelection);
}

void StashDialog::openDialog(StashDialog::Mode m)
{
    m_model->clear();

    switch (m) {
    case Mode::Stash:
    case Mode::StashKeepIndex:
    case Mode::StashUntrackIncluded:
        m_lineEdit->setPlaceholderText(i18n("Stash message (optional). Enter to confirm, Esc to leave."));
        m_currentMode = m;
        break;
    case Mode::StashPop:
    case Mode::StashDrop:
    case Mode::StashApply:
    case Mode::ShowStashContent:
        m_lineEdit->setPlaceholderText(i18n("Type to filter, Enter to pop stash, Esc to leave."));
        m_currentMode = m;
        getStashList();
        break;
    case Mode::StashApplyLast:
        applyStash({});
        return;
    case Mode::StashPopLast:
        popStash({});
        return;
    default:
        return;
    }
    reselectFirst();
    updateViewGeometry();
    setFocus();
    exec();
}

bool StashDialog::eventFilter(QObject *obj, QEvent *event)
{
    // catch key presses + shortcut overrides to allow to have ESC as application wide shortcut, too, see bug 409856
    if (event->type() == QEvent::KeyPress || event->type() == QEvent::ShortcutOverride) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (obj == m_lineEdit) {
            const bool forward2list = (keyEvent->key() == Qt::Key_Up) || (keyEvent->key() == Qt::Key_Down) || (keyEvent->key() == Qt::Key_PageUp)
                || (keyEvent->key() == Qt::Key_PageDown);
            if (forward2list) {
                QCoreApplication::sendEvent(m_treeView, event);
                return true;
            }

            if (keyEvent->key() == Qt::Key_Escape) {
                m_lineEdit->clear();
                keyEvent->accept();
                hide();
                return true;
            }
        } else {
            const bool forward2input = (keyEvent->key() != Qt::Key_Up) && (keyEvent->key() != Qt::Key_Down) && (keyEvent->key() != Qt::Key_PageUp)
                && (keyEvent->key() != Qt::Key_PageDown) && (keyEvent->key() != Qt::Key_Tab) && (keyEvent->key() != Qt::Key_Backtab);
            if (forward2input) {
                QCoreApplication::sendEvent(m_lineEdit, event);
                return true;
            }
        }
    }

    // hide on focus out, if neither input field nor list have focus!
    else if (event->type() == QEvent::FocusOut && !(m_lineEdit->hasFocus() || m_treeView->hasFocus())) {
        m_lineEdit->clear();
        hide();
        return true;
    }

    return QWidget::eventFilter(obj, event);
}

void StashDialog::slotReturnPressed()
{
    switch (m_currentMode) {
    case Mode::Stash:
        stash(false, false);
        break;
    case Mode::StashKeepIndex:
        stash(true, false);
        break;
    case Mode::StashUntrackIncluded:
        stash(false, true);
        break;
    case Mode::StashApply:
        applyStash(m_treeView->currentIndex().data(StashIndexRole).toByteArray());
        break;
    case Mode::StashPop:
        popStash(m_treeView->currentIndex().data(StashIndexRole).toByteArray());
        break;
    case Mode::StashDrop:
        dropStash(m_treeView->currentIndex().data(StashIndexRole).toByteArray());
        break;
    case Mode::ShowStashContent:
        showStash(m_treeView->currentIndex().data(StashIndexRole).toByteArray());
        break;
    default:
        break;
    }

    m_lineEdit->clear();
    hide();
}

void StashDialog::reselectFirst()
{
    QModelIndex index = m_proxyModel->index(0, 0);
    m_treeView->setCurrentIndex(index);
}

void StashDialog::sendMessage(const QString &message, bool warn)
{
    KTextEditor::Message *msg = new KTextEditor::Message(message, warn ? KTextEditor::Message::Warning : KTextEditor::Message::Positive);
    msg->setPosition(KTextEditor::Message::TopInView);
    msg->setAutoHide(3000);
    msg->setAutoHideMode(KTextEditor::Message::Immediate);
    msg->setView(m_mainWindow->activeView());
    m_mainWindow->activeView()->document()->postMessage(msg);
}

void StashDialog::stash(bool keepIndex, bool includeUntracked)
{
    QStringList args{QStringLiteral("stash"), QStringLiteral("-q")};

    if (keepIndex) {
        args.append(QStringLiteral("--keep-index"));
    }
    if (includeUntracked) {
        args.append(QStringLiteral("-u"));
    }

    if (!m_lineEdit->text().isEmpty()) {
        args.append(QStringLiteral("-m"));
        args.append(m_lineEdit->text());
    }

    auto git = m_gitwidget->gitprocess();
    auto gitWidget = m_gitwidget;
    if (!git) {
        return;
    }

    disconnect(git, &QProcess::finished, nullptr, nullptr);
    connect(git, &QProcess::finished, m_gitwidget, [gitWidget](int exitCode, QProcess::ExitStatus es) {
        disconnect(gitWidget->gitprocess(), &QProcess::finished, nullptr, nullptr);
        if (es != QProcess::NormalExit || exitCode != 0) {
            gitWidget->sendMessage(i18n("Failed to stash changes"), true);
        } else {
            gitWidget->getStatus();
            gitWidget->sendMessage(i18n("Changes stashed successfully."), false);
        }
    });
    git->setArguments(args);
    git->start();
}

void StashDialog::getStashList()
{
    auto git = m_gitwidget->gitprocess();
    if (!git) {
        return;
    }

    git->setArguments({QStringLiteral("stash"), QStringLiteral("list")});
    git->start();

    QList<QByteArray> stashList;
    if (git->waitForStarted() && git->waitForFinished(-1)) {
        if (git->exitStatus() == QProcess::NormalExit && git->exitCode() == 0) {
            stashList = git->readAllStandardOutput().split('\n');
        } else {
            m_gitwidget->sendMessage(i18n("Failed to get stash list. Error:\n %1", QString::fromUtf8(git->readAllStandardError())), true);
        }
    }

    // format stash@{}: message
    for (const auto &stash : stashList) {
        if (!stash.startsWith("stash@{")) {
            continue;
        }
        int brackCloseIdx = stash.indexOf('}', 7);

        if (brackCloseIdx < 0) {
            continue;
        }

        QByteArray stashIdx = stash.mid(0, brackCloseIdx + 1);

        QStandardItem *item = new QStandardItem(QString::fromUtf8(stash));
        item->setData(stashIdx, StashIndexRole);
        m_model->appendRow(item);
    }
}

void StashDialog::popStash(const QByteArray &index, const QString &command)
{
    auto git = m_gitwidget->gitprocess();
    auto gitWidget = m_gitwidget;
    if (!git) {
        return;
    }

    disconnect(git, &QProcess::finished, nullptr, nullptr);
    QStringList args{QStringLiteral("stash"), command};
    if (!index.isEmpty()) {
        args.append(QString::fromUtf8(index));
    }

    connect(git, &QProcess::finished, gitWidget, [gitWidget, command](int exitCode, QProcess::ExitStatus es) {
        disconnect(gitWidget->gitprocess(), &QProcess::finished, nullptr, nullptr);
        if (es != QProcess::NormalExit || exitCode != 0) {
            auto git = gitWidget->gitprocess();
            if (command == QLatin1String("apply")) {
                gitWidget->sendMessage(i18n("Failed to apply stash. Error:\n%1", QString::fromUtf8(git->readAllStandardError())), true);
            } else if (command == QLatin1String("drop")) {
                gitWidget->sendMessage(i18n("Failed to drop stash. Error:\n%1", QString::fromUtf8(git->readAllStandardError())), true);
            } else {
                gitWidget->sendMessage(i18n("Failed to pop stash. Error:\n%1", QString::fromUtf8(git->readAllStandardError())), true);
            }
        } else {
            gitWidget->getStatus();
            if (command == QLatin1String("apply")) {
                gitWidget->sendMessage(i18n("Stash applied successfully."), false);
            } else if (command == QLatin1String("drop")) {
                gitWidget->sendMessage(i18n("Stash dropped successfully."), false);
            } else {
                gitWidget->sendMessage(i18n("Stash popped successfully."), false);
            }
        }
    });
    git->setArguments(args);
    git->start();
}

void StashDialog::applyStash(const QByteArray &index)
{
    popStash(index, QStringLiteral("apply"));
}

void StashDialog::dropStash(const QByteArray &index)
{
    popStash(index, QStringLiteral("drop"));
}

void StashDialog::showStash(const QByteArray &index)
{
    if (index.isEmpty()) {
        return;
    }

    auto git = m_gitwidget->gitprocess();
    auto gitWidget = m_gitwidget;
    if (!git) {
        return;
    }

    disconnect(git, &QProcess::finished, nullptr, nullptr);
    QStringList args{QStringLiteral("stash"), QStringLiteral("show"), QStringLiteral("-p"), QString::fromUtf8(index)};

    connect(git, &QProcess::finished, gitWidget, [gitWidget](int exitCode, QProcess::ExitStatus es) {
        // sever connection
        disconnect(gitWidget->gitprocess(), &QProcess::finished, nullptr, nullptr);
        if (es != QProcess::NormalExit || exitCode != 0) {
            gitWidget->sendMessage(i18n("Failed to get Diff of file. Error:\n%1", QString::fromUtf8(gitWidget->gitprocess()->readAllStandardError())), true);
        } else {
            std::unique_ptr<QTemporaryFile> f(new QTemporaryFile);
            f->setFileTemplate(QString(QStringLiteral("XXXXXX.diff")));
            if (!f->open()) {
                return;
            }

            f->write(gitWidget->gitprocess()->readAllStandardOutput());
            f->flush();
            const QUrl tempFileUrl(QUrl::fromLocalFile(f->fileName()));
            auto v = gitWidget->mainWindow()->openUrl(tempFileUrl);
            if (!v || !v->document()) {
                return;
            }

            std::pair<std::unique_ptr<QTemporaryFile>, KTextEditor::View *> tf = std::make_pair(std::move(f), v);
            gitWidget->tempFilesVector()->push_back(std::move(tf));

            // close temp on document close
            auto clearTemp = [gitWidget](KTextEditor::Document *document) {
                auto tempFiles = gitWidget->tempFilesVector();
                std::remove_if(tempFiles->begin(), tempFiles->end(), [document](const GitWidget::TempFileViewPair &tf) {
                    if (tf.second->document() == document) {
                        return true;
                    }
                    return false;
                });
            };
            connect(v->document(), &KTextEditor::Document::aboutToClose, gitWidget, clearTemp);
        }
    });
    git->setArguments(args);
    git->start();
}

void StashDialog::updateViewGeometry()
{
    m_treeView->resizeColumnToContents(0);
    m_treeView->resizeColumnToContents(1);

    QWidget *window = m_mainWindow->window();
    const QSize centralSize = window->size();

    // width: 2.4 of editor, height: 1/2 of editor
    const QSize viewMaxSize(centralSize.width() / 2.4, centralSize.height() / 2);

    const QSize widgetSize(centralSize.width() / 2.4, m_model->rowCount() == 0 ? m_lineEdit->sizeHint().height() * 2 : centralSize.height() / 2);

    // Position should be central over window
    const int xPos = std::max(0, (centralSize.width() - viewMaxSize.width()) / 2);
    const int yPos = std::max(0, (centralSize.height() - viewMaxSize.height()) * 1 / 4);
    const QPoint p(xPos, yPos);
    move(p + window->pos());

    this->setFixedSize(widgetSize);
}
